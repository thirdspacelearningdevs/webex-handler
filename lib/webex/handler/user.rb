module Webex
  module Handler
    class User
      attr_accessor :username, :password,
                    :user_username, :user_password, :firstname, :lastname, :email,
                    :code,
                    :new_user_username
    
      def initialize(attributes = {})
        attributes.each do |k, v|
          send("#{k}=", v)
        end
      end
      
      # usage:
      # Webex::Handler::User.create_user(username: '[username]', password: '[password]', user_username: '[user_username]', user_password: '[user_password]', firstname: '[firstname]', lastname: '[lastname]', email: '[email]')
      #
      def self.create_user attrs = {}
        user = User.new(attrs)
        user.code = 200



        # create user
        request = CreateUser.new(
          username: user.username,
          password: user.password,
          user_username: user.user_username,
          user_password: user.user_password,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email
        )

        response = request.execute
        return response unless response.code == 200

        user
      end


      # usage:
      # Webex::Handler::User.set_user(username: '[username]', password: '[password]', user_username: '[user_username]',user_password: '[user_password]')
      #
      def self.set_user attrs = {}
        user = User.new(attrs)
        user.code = 200



        # create user
        request = SetUser.new(
          username: user.username,
          password: user.password,
          user_username: user.user_username,
          user_password: user.user_password
        )

        response = request.execute
        return response unless response.code == 200

        user
      end

      # usage:
      # Webex::Handler::User.set_user(username: '[username]', password: '[password]', user_username: '[user_username]', new_user_username: '[new_user_username]')
      #
      def self.set_user_username attrs = {}
        user = User.new(attrs)
        user.code = 200



        # update user username
        request = SetUserUsername.new(
          username: user.username,
          password: user.password,
          user_username: user.user_username,
          new_user_username: user.new_user_username
        )

        response = request.execute
        return response unless response.code == 200

        user
      end

      private

    end
  end
end