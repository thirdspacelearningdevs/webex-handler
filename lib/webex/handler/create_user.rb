module Webex
  module Handler
    class CreateUser < WebexRequest    

      attr_accessor :username, :password, :user_username, :user_password, :firstname, :lastname, :email

      def xml_body
        <<-XML
          <body>
            <bodyContent xsi:type="java:com.webex.service.binding.user.CreateUser">
              <firstName>#{firstname}</firstName>
              <lastName>#{lastname}</lastName>
              <webExId>#{user_username}</webExId>
              <email>#{email}</email>
              <password>#{user_password}</password>

              <privilege>
                <host>true</host>
              </privilege>
              <active>ACTIVATED</active>
              

              <sessionOptions>
                <defaultSessionType>101</defaultSessionType>
                <autoDeleteAfterMeetingEnd>false</autoDeleteAfterMeetingEnd>
              </sessionOptions>

            </bodyContent>
          </body>
        XML
      end

      private
    
    end
  end
end