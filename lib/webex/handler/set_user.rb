module Webex
  module Handler
    class SetUser < WebexRequest    

      attr_accessor :username, :password, :user_username, :user_password

      def xml_body
        <<-XML
          <body>
            <bodyContent xsi:type="java:com.webex.service.binding.user.SetUser">
              <webExId>#{user_username}</webExId>
              <password>#{user_password}</password>
            </bodyContent>
          </body>
        XML
      end

      private
    
    end
  end
end