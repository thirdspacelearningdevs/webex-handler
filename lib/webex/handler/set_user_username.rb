module Webex
  module Handler
    class SetUserUsername < WebexRequest    

      attr_accessor :username, :password, :user_username, :new_user_username

      def xml_body
        <<-XML
          <body>
            <bodyContent xsi:type="java:com.webex.service.binding.user.SetUser">
              <webExId>#{user_username}</webExId>
              <newWebExId>#{new_user_username}</newWebExId>
            </bodyContent>
          </body>
        XML
      end

      private
    
    end
  end
end